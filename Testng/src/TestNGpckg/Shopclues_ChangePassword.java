package TestNGpckg;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
public class Shopclues_ChangePassword {
	
	public WebDriver driver;
	
	@BeforeTest
	 @Parameters("browser")
	  public void beforeTest(@Optional("firefox") String browser) throws Exception {
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		ChromeOptions option=new ChromeOptions();
		option.addArguments("----disable-notification----");
		driver=new ChromeDriver(option);
		driver.manage().window().maximize();
		 }
		else if(browser.equalsIgnoreCase("firefox"))
		{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		FirefoxOptions option=new FirefoxOptions();
		option.addArguments("----disable-notification----");
		driver=new FirefoxDriver(option);
		driver.manage().window().maximize();
		}
		else
		{
		throw new Exception("Browser is not correct");
		}

		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
		
		 
	  }
	
	@Test(priority=0)
	public void Login() throws InterruptedException {
		
		driver.get("https://www.shopclues.com/");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		Thread.sleep(3000);
		
		  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arpu814+a4@gmail.com");
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arpu8144");
		  Thread.sleep(2000);
		  
		  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();

		  Actions actions=new Actions(driver);
		  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		  actions.moveToElement(element).perform();
		  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[5]/div/div/div[2]/div/div/div/div[1]/ul/li[4]/a")).click();
		  Thread.sleep(2000);
	}
  
	@Test(priority=1)
	public void blank_values() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(2000);
		
		String actual_result = driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]/span")).getText();
		 String expected_result = "Current password cannot be empty";
		 Assert.assertEquals(actual_result, expected_result);
	}
  
	@Test(priority=2)
	public void enter_currentpass_blank_newpass_confirmpass() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("arpu8144");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(2000);
		
		String actual_result = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
		 String expected_result = "New password cannot be empty";
		 Assert.assertEquals(actual_result, expected_result);
		
	}
  
	@Test(priority=3)
	public void enter_currentpass_newpass_blank_confirmpass() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).clear();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("arpu8144");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("arpu81444");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(2000);
		
		String actual_result = driver.findElement(By.xpath("//*[@id=\"password2Blank\"]/span")).getText();
		 String expected_result = "Confirm password cannot be empty";
		 Assert.assertEquals(actual_result, expected_result);
	}
  
	@Test(priority=4)
	public void enter_confirmpass_blank_cureent_newpass() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("arpu8144");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(2000);
		
		String actual_result = driver.findElement(By.xpath("//*[@id=\"passwordBlank\"]/span")).getText();
		 String expected_result = "Current password cannot be empty";
		 Assert.assertEquals(actual_result, expected_result);
	}
  
	@Test(priority=5)
	public void enter_currentpass_enter_numericvalue_neworconfirmpass() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password2\"]")).clear();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("arpu8144");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("8144444");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("8144444");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(2000);
		
		String actual_result = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
		 String expected_result = "Password must be alphanumeric";
		 Assert.assertEquals(actual_result, expected_result);
	}
  
	@Test(priority=6)
	public void enter_currentpass_enter_minimumlength_neworconfirmpass() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password2\"]")).clear();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("arpu8144");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("8144");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("8144");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(2000);
		
		String actual_result = driver.findElement(By.xpath("//*[@id=\"password1Blank\"]")).getText();
		 String expected_result = "Password must be 6 characters or more";
		 Assert.assertEquals(actual_result, expected_result);
	}
	
	@Test(priority=7)
	public void enter_currentpass_mismatch_neworconfirmpass() throws InterruptedException {
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).clear();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password2\"]")).clear();
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"passwordc\"]")).sendKeys("arpu8144");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password1\"]")).sendKeys("arpu81445");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password2\"]")).sendKeys("arpu8144");
		Thread.sleep(2000);
		
		driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		Thread.sleep(2000);
		
		String actual_result = driver.findElement(By.xpath("//*[@id=\"newEqualToConfirm\"]")).getText();
		 String expected_result = "New password and confirm new password do not match";
		 Assert.assertEquals(actual_result, expected_result);
		 Thread.sleep(5000);
		// Actions actions=new Actions(driver);
		// WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		// actions.moveToElement(element).perform();
		// driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/div/ul/li[10]/a")).click();
	}
  
	@AfterTest
	public void afterTest() {
		driver.close();
	}

}
