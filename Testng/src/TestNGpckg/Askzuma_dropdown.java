package TestNGpckg;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Askzuma_dropdown {
	
	public WebDriver driver;
	
	 @BeforeTest
	 @Parameters("browser")
	  public void beforeTest(@Optional("firefox") String browser) throws Exception {
		 	
		 if(browser.equalsIgnoreCase("chrome"))
			{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
			ChromeOptions option=new ChromeOptions();
			option.addArguments("----disable-notification----");
			driver=new ChromeDriver(option);
			driver.manage().window().maximize();
			 }
			else if(browser.equalsIgnoreCase("firefox"))
			{
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\geckodriver.exe");
			FirefoxOptions option=new FirefoxOptions();
			option.addArguments("----disable-notification----");
			driver=new FirefoxDriver(option);
			driver.manage().window().maximize();
			}
			else
			{
			throw new Exception("Browser is not correct");
			}

			driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
			
	  }

	 @Test(priority=0)
	 public void dropdown() throws InterruptedException {
		 
		 	driver.get("https://askzuma.com/");
			Thread.sleep(3000);
		 
		 	driver.findElement(By.xpath("//*[@id=\"select2-chosen-1\"]")).click();
			driver.findElement(By.id("select2-result-label-8")).click();
			
			Thread.sleep(3000);
			
			driver.findElement(By.xpath("//*[@id=\"select2-chosen-46\"]")).click();
			
			driver.findElement(By.id("select2-result-label-47")).click();
			
			Thread.sleep(5000);
			
			driver.findElement(By.xpath("//*[@id=\"select2-chosen-90\"]")).click();
			
			driver.findElement(By.id("select2-result-label-91")).click();
			
			Thread.sleep(5000);
			
			driver.findElement(By.xpath("//*[@id=\"select2-chosen-93\"]")).click();
			
			driver.findElement(By.id("select2-result-label-94")).click();
			Thread.sleep(5000);
			
			driver.findElement(By.id("selectJob")).click();
			
			String actual_resut=driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div/div/div[2]")).getText();
			String expected_result="Please specify the the location.";
			Assert.assertEquals(actual_resut, expected_result);
			Thread.sleep(4000);
			
	 }
 
	 @AfterTest
	 public void afterTest() {
		 driver.close();
	 }

}
