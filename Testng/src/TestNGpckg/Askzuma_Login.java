package TestNGpckg;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
public class Askzuma_Login {
	public WebDriver driver;
	
 @BeforeTest
 @Parameters("browser")
 public void beforeTest(@Optional("firefox") String browser) throws Exception {
	 if(browser.equalsIgnoreCase("chrome"))
		{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		ChromeOptions option=new ChromeOptions();
		option.addArguments("----disable-notification----");
		driver=new ChromeDriver(option);
		driver.manage().window().maximize();
		 }
		else if(browser.equalsIgnoreCase("firefox"))
		{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		FirefoxOptions option=new FirefoxOptions();
		option.addArguments("----disable-notification----");
		driver=new FirefoxDriver(option);
		driver.manage().window().maximize();
		}
		else
		{
		throw new Exception("Browser is not correct");
		}

		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
		Thread.sleep(5000);

}
	
 
 
  @Test (priority =0)
  public void bothvalues_blank() throws InterruptedException {
	  driver.get("https://askzuma.com/");
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	  
	   String actual_result = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	   String expected_result = "Required";
	   Assert.assertEquals(actual_result, expected_result);
	   
	   String actual_result1 = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
	   String expected_result1 = "Required";
	   Assert.assertEquals(actual_result1, expected_result1);
	   }
  
  @Test (priority =1)
  public void password_blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("a123@gmail.com");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click(); 
	  Thread.sleep(3000);
	  
	  String actual_result = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
	   String expected_result = "Required";
	   Assert.assertEquals(actual_result, expected_result);
	 
  }
  
  @Test (priority =2)
  public void emailid_blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("a123");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click(); 
	  Thread.sleep(3000);
	  
	  String actual_result = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	   String expected_result = "Required";
	   Assert.assertEquals(actual_result, expected_result);
	  
  }
  
 
  @Test (priority =3)
  public void email_pass_invalid() throws InterruptedException {
	  
	 // driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).clear();
	  
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("arpu123@gmailcom");
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("a123");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click(); 
	  Thread.sleep(3000);
	  
	  String actual_result = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	   String expected_result = "Invalid format";
	   Assert.assertEquals(actual_result, expected_result);
  }

  @Test (priority =4)
  public void valid_email_pass() throws InterruptedException {
	  
	   driver.navigate().refresh();
	   driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	   
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).clear();
	  
	  driver.findElement(By.xpath("//*[@id=\"Email\"]")).sendKeys("ankur1.manish@gmail.com");
	  driver.findElement(By.xpath("//*[@id=\"Password\"]")).sendKeys("12345678");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click(); 
	  Thread.sleep(3000);
 }
  
  @Test (priority =5)
  public void search() throws InterruptedException {
  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul/li[2]/a")).click(); 
	  Thread.sleep(2000);
	  driver.findElement(By.id("tbLocation")).sendKeys("USA");
	  Thread.sleep(2000);
	  driver.findElement(By.id("tbShopName")).sendKeys("Euro Motocars");
	  Thread.sleep(2000);
	  driver.findElement(By.id("btnSearchShops")).click();
	  Thread.sleep(5000);
  }
  
  @Test (priority =6)
  public void logout() throws InterruptedException {
  
	  driver.findElement(By.xpath("/html/body/div[7]/header/div/nav/a")).click(); 
	  Thread.sleep(3000);
  }
	  
  @AfterTest
  public void afterTest() {
	  driver.close();
  }

}
