package TestNGpckg;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Guru99_VerifyPageTitle {
	
	public WebDriver driver;
	
	@BeforeTest
	public void beforeTest() throws InterruptedException {
		
		 System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		 driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 driver.get("http://demo.guru99.com/test/newtours/");
		 Thread.sleep(3000);
	}

	
	@Test
	public void getPageTitle_verifyPagetitle() {
		
		 String actualTitle = driver.getTitle();
		 System.out.println("Actual Title Is: " + actualTitle);
		 Assert.assertTrue(actualTitle.contains("Welcome: Mercury Tours"));
		 String actual_Result=driver.getTitle();
		 String expect_Result="Welcome: Mercury Tours";
		 Assert.assertEquals(actual_Result, expect_Result);
	}
  
	@AfterTest
  	public void afterTest() {
	}

}
