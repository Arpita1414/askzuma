package TestNGpckg;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Shopclues_AddItem {
	
	public WebDriver driver;
	
	@BeforeTest
	 @Parameters("browser")
	  public void beforeTest(@Optional("firefox") String browser) throws Exception {
		
		if(browser.equalsIgnoreCase("chrome"))
		{
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		ChromeOptions option=new ChromeOptions();
		option.addArguments("----disable-notification----");
		driver=new ChromeDriver(option);
		driver.manage().window().maximize();
		 }
		else if(browser.equalsIgnoreCase("firefox"))
		{
		System.setProperty("webdriver.gecko.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\geckodriver.exe");
		FirefoxOptions option=new FirefoxOptions();
		option.addArguments("----disable-notification----");
		driver=new FirefoxDriver(option);
		driver.manage().window().maximize();
		}
		else
		{
		throw new Exception("Browser is not correct");
		}

		driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);
		
	  }

	@Test(priority=0)
	public void newtab_additem() throws InterruptedException {
		
		driver.get("https://www.shopclues.com/");
		Thread.sleep(3000);
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scrollBy(0, 700)");
		Thread.sleep(5000);
		
		driver.findElement(By.id("127779866")).click();
		Thread.sleep(3000);
		
			ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
	        driver.switchTo().window(tabs.get(1));
			driver.get("https://www.shopclues.com/usb-optical-wired-mouse-black-127779866.html");
		
			Thread.sleep(3000);
			
			JavascriptExecutor js1 = (JavascriptExecutor)driver;
			js1.executeScript("scrollBy(0, 500)");
			Thread.sleep(2000);
			driver.findElement(By.id("add_cart")).click();
			Thread.sleep(2000);
			
			Actions action=new Actions(driver);
			WebElement element=driver.findElement(By.xpath("/html/body/div[3]/div/div/div[4]/ul/li[4]/a"));
			action.moveToElement(element).perform();
			Thread.sleep(2000);

			driver.findElement(By.xpath("/html/body/div[3]/div/div/div[4]/ul/li[4]/div/div/div[3]/a[1]")).click();
			Thread.sleep(2000);
			
			String actual_result = driver.findElement(By.xpath("//*[@id=\"gt-cart-price\"]/p[2]/span")).getText();
			String expected_result = "Rs 208";
			Assert.assertEquals(actual_result, expected_result);
			Thread.sleep(2000);
			
			driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/span/a[2]")).click();                        
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/span/a[1]")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"127779866_2666290063\"]/div[2]/div[2]/a")).click();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[@id=\"loadContent\"]/div[1]/div[2]/div[2]/a[1]")).click();
			Thread.sleep(5000);
			driver.close();
			driver.switchTo().window(tabs.get(0));
			driver.close();
	}
	
	
  
	@AfterTest
	public void afterTest() throws InterruptedException {
		
	}

}
