package TestNGpckg;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Shopclues_Login {
	
	public WebDriver driver;
	
  @BeforeTest
  public void beforeTest() throws InterruptedException {
	  	System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.shopclues.com/");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		Thread.sleep(3000);
  }
	
	
  @Test(priority=0)
  public void both_value_blank() {
	  
	  	driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  	
	  	String actual_result = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
		   String expected_result = "Please enter email id or mobile number.";
		   Assert.assertEquals(actual_result, expected_result);
		   
		   String actual_result1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
		   String expected_result1 = "Please enter your password.";
		   Assert.assertEquals(actual_result1, expected_result1);
  }
  
  @Test(priority=1)
  public void pass_blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arpu814+a4@gmail.com");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(3000);
	  
	  String actual_result1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
	  String expected_result1 = "Please enter your password.";
	  Assert.assertEquals(actual_result1, expected_result1);
  }
  
  @Test(priority=2)
  public void email_mobile_blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arpu8144");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(3000);
	  
	  String actual_result1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
	  String expected_result1 = "Please enter email id or mobile number.";
	  Assert.assertEquals(actual_result1, expected_result1);
	  
  }
  
  @Test(priority=3)
  public void invalid_email_mobile() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arpu814gmail.com");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arpu8144");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(3000);
	  
	  String actual_result1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[2]/div[1]/span")).getText();
	  String expected_result1 = "Please enter valid email id or mobile number.";
	  Assert.assertEquals(actual_result1, expected_result1);
  }
  
  @Test(priority=4)
  public void invalid_pass() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arpu814@gmail.com");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arpu814");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(3000);
	  
	  String actual_result1 = driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/div[1]/span")).getText();
	  String expected_result1 = "Incorrect login details.";
	  Assert.assertEquals(actual_result1, expected_result1);
  }
  
  @Test(priority=5)
  public void valid_email_pass() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).clear();
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).clear();
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arpu814+a4@gmail.com");
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arpu8144");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();

	  Actions actions=new Actions(driver);
	  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
	  actions.moveToElement(element).perform();
	  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[10]/a")).click();
  }
  
  
  @AfterTest
  public void afterTest() {
  }

}
