package TestNGpckg;
import org.openqa.selenium.*;


import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.chrome.*;

public class Shopclues_Registration {
	public WebDriver driver;
	
 @BeforeTest
 public void beforeTest() throws InterruptedException {
	 
	 	System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.shopclues.com/");
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
		Thread.sleep(3000);
		//driver.findElement(By.xpath("//*[@id=\"reg_tab\"]")).click();
		//Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
		
		
 }
 
  @Test(priority=0)
  public void Both_values_blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"reg_tab\"]")).click();
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  Thread.sleep(3000);
	  
	   String actual_result = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	   String expected_result = "Please enter your email id.";
	   Assert.assertEquals(actual_result, expected_result);
	   
	   String actual_result1 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/div/span")).getText();
	   String expected_result1 = "Please enter your mobile number.";
	   Assert.assertEquals(actual_result1, expected_result1);
	   //Thread.sleep(3000);
	  /* String actual_result2 = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[4]/div/span")).getText();
	   String expected_result2 = "Please enter your password.";
	   Assert.assertEquals(actual_result2, expected_result2);*/
  }
 
  @Test(priority =1)
  public void mobileno_blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("arpu814+a1@gmail.com");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  Thread.sleep(3000);
	  
	  String actual_result = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/div/span")).getText();
	  String expected_result= "Please enter your mobile number.";
	  Assert.assertEquals(actual_result, expected_result);
	//  Thread.sleep(3000);
  }
 
  @Test(priority =2)
  public void email_blank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).clear();
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("9855994852");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  Thread.sleep(2000);
	  
	  String actual_result = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expected_result= "Please enter your email id.";
	  Assert.assertEquals(actual_result, expected_result);
	  
  }
  /*@Test
  public void pass_blank() {
  }*/
 
  @Test(priority=3)
  public void invalid_email() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).clear();
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("arp@gmailcom");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("1234567890");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  Thread.sleep(2000);
	  
	  String actual_result = driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/div/span")).getText();
	  String expected_result= "Invalid email id.";
	  Assert.assertEquals(actual_result, expected_result);	  
	  
  }
  
  @Test(priority=4)
  public void invalid_mobileno() throws InterruptedException {
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).clear();
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).clear();
	  Thread.sleep(2000);
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[2]/input")).sendKeys("arp@gmail.com");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/input")).sendKeys("123456789");
	  Thread.sleep(2000);
	  
	  driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[6]/div/a")).click();
	  Thread.sleep(2000);
	  
	  String actual_result=driver.findElement(By.xpath("//*[@id=\"register\"]/form/fieldset/div[3]/div/span")).getText();
	  String expected_result="Invalid mobile number.";
	  Assert.assertEquals(actual_result,expected_result );
  }
 
  @Test
  public void f() {
  }
 
  @Test
  public void f1() {
  }
 
  @AfterTest
  public void afterTest() {
  }

}
