package TestNGpckg;
import org.openqa.selenium.*;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.openqa.selenium.chrome.*;
public class DateTimePicker {
	
	public WebDriver driver;
	
	@BeforeTest
	  	public void beforeTest() throws InterruptedException {
		
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("http://demo.guru99.com/test/");
			Thread.sleep(3000);
		}
  
	@Test(priority=0)
		public void datetime_picker() throws InterruptedException {
		
			WebElement element=driver.findElement(By.xpath("/html/body/form/input[1]"));
			element.sendKeys("22122020");
			element.sendKeys(Keys.TAB);
			element.sendKeys("1453PM");
			driver.findElement(By.xpath("/html/body/form/input[2]")).click();
			Thread.sleep(2000);
			
			String actual_result=driver.findElement(By.xpath("/html/body/div[2]")).getText();
			String expected_result="Your Birth Date is 2020-12-22\n"
					+ "Your Birth Time is 14:53";
			Assert.assertEquals(actual_result, expected_result);
			
		}
  

	@AfterTest
		public void afterTest() {
		}

}
