package TestNGpckg;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Shopcluse_Editprofile {
	
	public WebDriver driver;
	
	 @BeforeTest
	 @Parameters("browser")
	  public void beforeTest(@Optional("firefox") String browser) throws Exception {
		 
		 if(browser.equalsIgnoreCase("chrome"))
			{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\chromedriver.exe");
			ChromeOptions option=new ChromeOptions();
			option.addArguments("----disable-notification----");
			driver=new ChromeDriver(option);
			driver.manage().window().maximize();
			 }
			else if(browser.equalsIgnoreCase("firefox"))
			{
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\HP\\OneDrive\\Desktop\\Selenium Webdriver\\geckodriver.exe");
			FirefoxOptions option=new FirefoxOptions();
			option.addArguments("----disable-notification----");
			driver=new FirefoxDriver(option);
			driver.manage().window().maximize();
			}
			else
			{
			throw new Exception("Browser is not correct");
			}

			driver.manage().timeouts().implicitlyWait(2000, TimeUnit.SECONDS);

			}
			
	  
	 
	 @Test(priority=0)
	 
	 public void valid_email_pass() throws InterruptedException {
		 
		 driver.get("https://www.shopclues.com/");
			Thread.sleep(5000);
			driver.findElement(By.xpath("//*[@id=\"sign-in\"]/a")).click();
			Thread.sleep(3000);
		 
		  driver.findElement(By.xpath("//*[@id=\"main_user_login\"]")).sendKeys("arpu814+a4@gmail.com");
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id=\"login\"]/form/fieldset/div[3]/input")).sendKeys("arpu8144");
		  Thread.sleep(2000);
		  
		  driver.findElement(By.xpath("//*[@id=\"login_button\"]")).click();
		  Thread.sleep(3000);
		  driver.findElement(By.xpath("//*[@id=\"loginModelBox\"]/div/div[2]/div[11]/div/form/div[3]/div/a")).click();

		  Actions actions=new Actions(driver);
		  WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		  actions.moveToElement(element).perform();
		  Thread.sleep(2000);
		  driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
		  Thread.sleep(2000);
		 
	 }
	 
	 @Test(priority=1)
	 public void All_values_blank() throws InterruptedException {
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		 Thread.sleep(2000);
		 
		 String actual_result = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
		 String expected_result = "First name cannot be blank";
		 Assert.assertEquals(actual_result, expected_result);
	 }
	 
	 @Test(priority=2)
	 public void enter_firstname_blank_lastname_phoneno() throws InterruptedException {
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Arpuuu");
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		 Thread.sleep(2000);
		 
		 String actual_result = driver.findElement(By.xpath("//*[@id=\"checkBlank2\"]/span")).getText();
		 String expected_result = "Last name cannot be blank";
		 Assert.assertEquals(actual_result, expected_result);
	 }
	 
	 @Test(priority=3)
	 public void enter_firstname_lastname_blank_phoneno() throws InterruptedException {
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Arpuuu");
		 //Thread.sleep(2000);
		 driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("kadddiaaa");
		// Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		 Thread.sleep(2000);
		 
		 String actual_result = driver.findElement(By.xpath("//*[@id=\"checkBlank3\"]/span")).getText();
		 String expected_result = "Mobile number cannot be blank";
		 Assert.assertEquals(actual_result, expected_result);
		 
	 }
	 
	 @Test(priority=4)
	 public void enter_phoneno_blank_firstname_lastname() throws InterruptedException {
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("9727681110");
		// Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		 Thread.sleep(2000);
		 
		 String actual_result = driver.findElement(By.xpath("//*[@id=\"checkBlank1\"]/span")).getText();
		 String expected_result = "First name cannot be blank";
		 Assert.assertEquals(actual_result, expected_result);
		 
	 }
	 
	 @Test(priority=5)
	 public void invalid_firstname_lastname_phoneno() throws InterruptedException {
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("1549g5+5");
		// Thread.sleep(2000);
		 driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("48ojiohig");
		// Thread.sleep(2000);
		 driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("jbibi659+");
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		 Thread.sleep(2000);
		 
		 String actual_result = driver.findElement(By.xpath("//*[@id=\"fname\"]")).getText();
		 String expected_result = "Enter correct first name";
		 Assert.assertEquals(actual_result, expected_result);
	 }
	 
	 @Test(priority=6)
	 public void valid_fname_lname_phoneno() throws InterruptedException {
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"lastname\"]")).clear();
		 driver.findElement(By.xpath("//*[@id=\"phone\"]")).clear();
		 Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"firstname\"]")).sendKeys("Arpuu");
		// Thread.sleep(2000);
		 driver.findElement(By.xpath("//*[@id=\"lastname\"]")).sendKeys("kadiaa");
		 //Thread.sleep(2000);
		 driver.findElement(By.xpath("//*[@id=\"phone\"]")).sendKeys("9727681110");
		// Thread.sleep(2000);
		 
		 driver.findElement(By.xpath("//*[@id=\"save_profile_but\"]")).click();
		 Thread.sleep(2000);
		 
		 Actions actions=new Actions(driver);
		 WebElement element=driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a"));
		 actions.moveToElement(element).perform();
		 Thread.sleep(7000);
		 driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[10]/a")).click();
	 }
 
	 @AfterTest
	 public void afterTest() {
	 }

}
